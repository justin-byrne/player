"use strict";
// import { getRandomInt, log } from './helpers/Helpers';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function log(msg) {
    return console.log(msg);
}
function displayTable(msg) {
    console.table(msg);
}
var Attributes = /** @class */ (function () {
    function Attributes(intelligence, strength, vitality, dexterity, luck) {
        this._intelligence = intelligence;
        this._strength = strength;
        this._vitality = vitality;
        this._dexterity = dexterity;
        this._luck = luck;
    }
    return Attributes;
}());
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    /// CONSTRUCTORS
    function Player(name, classification, iterator, RNG) {
        var _this = _super.call(this, getRandomInt(RNG), getRandomInt(RNG), getRandomInt(RNG), getRandomInt(RNG), getRandomInt(RNG)) || this;
        _this._name = name;
        _this._iterator = iterator;
        _this._classifcation = classification;
        return _this;
    }
    Object.defineProperty(Player.prototype, "name", {
        /// GETTERS
        get: function () {
            return this._name;
        },
        /// SETTERS
        set: function (name) {
            this._name = name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "iterator", {
        get: function () {
            return this._iterator;
        },
        set: function (iterator) {
            this._iterator = iterator;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "classifcation", {
        get: function () {
            return this._classifcation;
        },
        set: function (classifcation) {
            this._classifcation = classifcation;
        },
        enumerable: true,
        configurable: true
    });
    Player.prototype.showStats = function () {
        log("Attributes: INT: " + this._intelligence + ", STR: " + this._strength + ", VIT: " + this._vitality + ", DEX: " + this._dexterity + ", LCK: " + this._luck);
    };
    return Player;
}(Attributes));
// export { Attributes, Player }
var p1 = new Player('Wamo', 1, 'Knight', 6);
displayTable(p1);
//# sourceMappingURL=index.js.map