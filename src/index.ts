// import { getRandomInt, log } from './helpers/Helpers';

function getRandomInt(max: number) 
{
    return Math.floor(Math.random() * Math.floor(max));
}

function log(msg: number | string) 
{
    return console.log(msg);
}

function displayTable(msg: object)
{
    console.table(msg);
}

class Attributes 
{
    _intelligence!: number
    _strength!:     number
    _vitality!:     number
    _dexterity!:    number
    _luck!:         number

    constructor(intelligence: number, strength: number, vitality: number, dexterity: number, luck: number)
    {
        this._intelligence = intelligence;
        this._strength     = strength;
        this._vitality     = vitality;
        this._dexterity    = dexterity;
        this._luck         = luck;
    }
}

class Player extends Attributes
{
    _name!:          string
    _classifcation!: string
    _iterator!:      number

    /// CONSTRUCTORS
    constructor(name: string, classification: string, iterator: number, RNG: number)
    {
        super(getRandomInt(RNG), getRandomInt(RNG), getRandomInt(RNG), getRandomInt(RNG), getRandomInt(RNG));

        this._name          = name;
        this._iterator      = iterator;
        this._classifcation = classification;
    }

    /// SETTERS
    set name(name: string)
    {
        this._name = name;
    }

    set iterator(iterator: number)
    {
        this._iterator = iterator;
    }

    set classifcation(classifcation: string)
    {
        this._classifcation = classifcation;
    }

    /// GETTERS
    get name()
    {
        return this._name;
    }

    get iterator()
    {
        return this._iterator;
    }

    get classifcation()
    {
        return this._classifcation;
    }

    showStats()
    {
        printLog(`Attributes: INT: ${this._intelligence}, STR: ${this._strength}, VIT: ${this._vitality}, DEX: ${this._dexterity}, LCK: ${this._luck}`)
    }
}

// export { Attributes, Player }

let p1 = new Player('Wamo', 1, 'Knight', 6);

displayTable(p1);
