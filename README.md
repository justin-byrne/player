# Directory Structure for TypeScript Application

    root
     |... README.md
     |... tsconfig.json
     |
     +--- app\
     |     |... _README.md
     |     |... main.ts
     |     |
     |     +--- classes\
     |     |     |... _README.md
     |     |     |... classA.ts   
     |     |
     |     +--- controllers\
     |     |     |... _README.md
     |     |     |... controllerA.ts
     |     |
     |     \--- helpers\
     |           |... _README.md
     |           |... helperA.ts
     |     
     +--- dist\
     |     |... _README.md
     |     |... index.js
     |
     +--- libs\
     |     |... _README.md
     |     |... my-lib.d.ts
     |
     +--- plugins\
     |     |... _README.md
     |     |... my-plugin.d.ts
     |
     \--- src\
           |... _README.md
           |... index.ts

