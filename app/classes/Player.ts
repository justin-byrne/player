///     IMPORTS
////////////////////////////////////////////////////////////////////////////////
import { IPlayer } from '../interfaces/IPlayer'
import { getRandomInt } from '../helpers/Helpers'

///     CLASS
////////////////////////////////////////////////////////////////////////////////
class Player implements IPlayer
{
    _name: string;    
    _classifcation: string;
    _iterator: number;
    _intelligence: number;
    _strength: number;
    _vitality: number;
    _dexterity: number;
    _luck: number;
    
    /// CONSTRUCTORS
    constructor(name: string, classification: string, iterator: number, RNG: number)
    {
        this._name          = name;
        this._iterator      = iterator;
        this._classifcation = classification;
        this._intelligence  = getRandomInt(RNG);
        this._strength      = getRandomInt(RNG);
        this._vitality      = getRandomInt(RNG);
        this._dexterity     = getRandomInt(RNG);
        this._luck          = getRandomInt(RNG);
    }

    /// SETTERS
    set name(name: string)
    {
        this._name = name;
    }

    set iterator(iterator: number)
    {
        this._iterator = iterator;
    }

    set classifcation(classifcation: string)
    {
        this._classifcation = classifcation;
    }

    set intelligence(intelligence: number)
    {
        this._intelligence = intelligence;
    }

    set strength(strength: number)
    {
        this._strength = strength;
    }

    set vitality(vitality: number)
    {
        this._vitality = vitality;
    }

    set dexterity(dexterity: number)
    {
        this._dexterity = dexterity;
    }

    set luck(luck: number)
    {
        this._luck = luck;
    }

    /// GETTERS
    get name()
    {
        return this._name;
    }

    get iterator()
    {
        return this._iterator;
    }

    get classifcation()
    {
        return this._classifcation;
    }

    get intelligence()
    {
        return this._intelligence;
    }

    get strength()
    {
        return this._strength;
    }

    get vitality()
    {
        return this._vitality;
    }

    get dexterity()
    {
        return this._dexterity;
    }

    get luck()
    {
        return this._luck;
    }
}

///     EXPORTS
////////////////////////////////////////////////////////////////////////////////
export { Player }