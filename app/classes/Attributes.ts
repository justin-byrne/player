///     IMPORTS
////////////////////////////////////////////////////////////////////////////////
import { IAttributes } from '../interfaces/IAttributes'

///     CLASS
////////////////////////////////////////////////////////////////////////////////
class Attributes implements IAttributes
{
    _intelligence!: number;    
    _strength!:     number;
    _vitality!:     number;
    _dexterity!:    number;
    _luck!:         number;

    constructor(intelligence: number, strength: number, vitality: number, dexterity: number, luck: number)
    {
        this._intelligence = intelligence;
        this._strength     = strength;
        this._vitality     = vitality;
        this._dexterity    = dexterity;
        this._luck         = luck;
    }
}

///     EXPORTS
////////////////////////////////////////////////////////////////////////////////
export { Attributes }