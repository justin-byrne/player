///     HELPERS
////////////////////////////////////////////////////////////////////////////////

function getRandomInt(max: number) 
{
    return Math.floor(Math.random() * Math.floor(max));
}

function printLog(msg: number | string) 
{
    console.log(msg);
}

function printTable(msg: object)
{
    console.table(msg);
}

///     EXPORTS
////////////////////////////////////////////////////////////////////////////////
export { getRandomInt, printLog, printTable }