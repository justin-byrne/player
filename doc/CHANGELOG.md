# Changelog
All notable changes to this project will be documented in this file.

## [0.12.5] - 2019-09-04
### Added
- Added `Getters` & `Setters` within the *Vitals* **class**
- Added `Helper.getRandomInt` method to *Vitals*, *Grim*, and *Organic* **classes**
- Added `Constructor` and `Getters` & `Setters` within the *Inventory*, *Abilities*, *AnatomyHuman*, *Vulnerability* **class**

### Changed
- Inverted `Getters` & `Setters` priority within *Character*, *Grim*, *Organic*, *Skill*, *Soul*, **classes**
- Changed *Books*, *Weapons*, *Sheild*, and *Accessories* from `Objects` to `Arrays`
- Changed Dark to Grim within *Vulernabilities* and *Abilities* **classes**

### Fixed
- Fixed *IInventroy* **interface** and *Inventory* **class** encapsulated object from upper case (Inventory) to lowercase (inventory)
- Fixed *Vulernability* and *Abilities* **classes**, aling with their corresponding **interfaces** to represent it's internal objects appriately title-cased

### Removed
- Removed `constructor` comments within the *IIventory* **Interface** 

---

## [0.9.0] - 2019-09-02
### Added
- Added base **classes** *Abilities*, *AnatomyHuman*, *Character*, *Grim*, *Inventory*, *Organic*, *Skill*, *Soul*, *Vitals*, *Vulnerability*
- Added base **interfaces** *_Interfaces*, *IAbilities*, *IAnatomyHuman*, *ICharacter*, *IGrim*, *IInventory*, *IOrganic*, *ISkill*, *ISoul*, *IVitals*, *IVulnerability*

---

[0.12.5]: 2019-09-04 [Unreleased]

[0.9.0]: 2019-09-02 [36816eb](https://bitbucket.org/justinbyrne001/grim/commits/2d2a6e3) Initial upload

---

## Types of changes
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.